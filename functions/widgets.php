<?php

register_sidebar([
  'name'          => __('Gifitoo', 'gifitoo'),
  'id'            => 'gifitoo-button-widget',
  'before_widget' => '<div class="gifitoo-button-wrapper gifitoo-widget">',
  'after_widget'  => '</div>',
  'before_title'  => '',
  'after_title'   => ''
]);
