<?php

class gifitoo_button_widget extends WP_Widget {

    /** constructor -- name this the same as the class above */
    function gifitoo_button_widget() {
        parent::WP_Widget(false, $name = 'Gifitoo WP Button');
    }

    /** @see WP_Widget::widget -- do not rename this */
    function widget($args, $instance) {

      extract( $args );

      $shopurl 	= $instance['shopurl'];
      $tooltip 	= $instance['tooltip'];
      $showtooltip = $instance[ 'showtooltip' ] ? 'on' : '';

      echo $before_widget;

      if($shopurl) :

        if(file_exists(plugin_dir_path( __FILE__ ) . '../assets/img/giftcard.svg')) :
          echo "<a class='gifitoo-button' href='".$shopurl."' target='_blank'>".file_get_contents(plugin_dir_path( __FILE__ ) . '../assets/img/giftcard.svg')."</a>";
        endif;

        if($showtooltip == 'on' && $tooltip) :
          echo "<div class='gifitoo-tooltip'>".$tooltip." <span class='angle'></span> <a data-tooltip-close class='close-button'>&Cross;</a></div>";
        endif;

      endif;

      echo $after_widget;

    }

    /** @see WP_Widget::update -- do not rename this */
    function update($new_instance, $old_instance) {
  		$instance = $old_instance;
  		$instance['shopurl'] = strip_tags($new_instance['shopurl']);
  		$instance['tooltip'] = strip_tags($new_instance['tooltip']);
  		$instance[ 'showtooltip' ] = $new_instance[ 'showtooltip' ];

      return $instance;
    }

    /** @see WP_Widget::form -- do not rename this */
    function form($instance) {

        $shopurl	= esc_attr($instance['shopurl']);
        $tooltip	= esc_attr($instance['tooltip']);

        ?>
		    <p>
          <label for="<?php echo $this->get_field_id('shopurl'); ?>"><?php _e('Gifitoo Shop URL'); ?></label>
          <input class="widefat" id="<?php echo $this->get_field_id('shopurl'); ?>" name="<?php echo $this->get_field_name('shopurl'); ?>" type="text" value="<?php echo $shopurl; ?>" />
        </p>
        <hr>
        <p>
          <label for="<?php echo $this->get_field_id('showtooltip'); ?>"><?php _e('Show tooltip'); ?></label>
          <input class="checkbox" type="checkbox" <?php checked( $instance[ 'showtooltip' ], 'on' ); ?> id="<?php echo $this->get_field_id( 'showtooltip' ); ?>" name="<?php echo $this->get_field_name( 'showtooltip' ); ?>" />
        </p>
		    <p>
          <label for="<?php echo $this->get_field_id('tooltip'); ?>"><?php _e('Tooltip Message'); ?></label>
          <input class="widefat" id="<?php echo $this->get_field_id('tooltip'); ?>" name="<?php echo $this->get_field_name('tooltip'); ?>" type="text" value="<?php echo $tooltip; ?>" />
        </p>

        <?php
    }


} // end class gifitoo_button_widget
add_action('widgets_init', create_function('', 'return register_widget("gifitoo_button_widget");'));
