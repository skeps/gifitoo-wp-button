<?php
/*
Plugin Name: Gifitoo WP Button
Plugin URI:  https://www.skeps.nl
Description: Adds a button to the website for linking to the Gifitoo shop
Version:     1.2
Author:      Skeps Mediabureau
Author URI:  https://www.skeps.nl
*/

include( plugin_dir_path( __FILE__ ) . 'functions/widgets.php');
include( plugin_dir_path( __FILE__ ) . 'widget/gifitoo-button.php');


add_action('wp_enqueue_scripts', 'callback_for_setting_up_scripts');
function callback_for_setting_up_scripts() {
  
    wp_enqueue_script( 'jscookie',  plugins_url('vendor/js.cookie/js.cookie.js', __FILE__ ), array( 'jquery' ) );

    wp_register_style( 'gifitoo_button', plugins_url('assets/css/style.css', __FILE__ ));
    wp_enqueue_style( 'gifitoo_button' );

    wp_enqueue_script( 'gifitoo_button',  plugins_url('assets/js/gifitoo.js', __FILE__ ), array( 'jquery' ) );

}

add_action( 'wp_footer', 'gifitoo_button' );
function gifitoo_button() {

  dynamic_sidebar('gifitoo-button-widget');

}
