jQuery(document).ready(function($){

  //Showing tooltip on mouseenter
  $('.gifitoo-button').on('mouseenter', function(){

    $(this).parent().find('.gifitoo-tooltip').fadeIn();

  });

  if(!Cookies.get('gifitoo-tooltip') || Cookies.get('gifitoo-tooltip') !== 'closed') {
    $('.gifitoo-tooltip').fadeIn();
  }


  //Closing tooltip
  $('.gifitoo-tooltip a[data-tooltip-close]').on('click', function(){

    $(this).parent().fadeOut();

    Cookies.set('gifitoo-tooltip', 'closed', { expires: 7 });

  });

});
